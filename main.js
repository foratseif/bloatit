function html_load_r_title(){
    let split = window.location.pathname.split("/")
    let subreddit = (split[1] == "r") && (split[2].length > 0) ? split[2] : "all" ;

    let h1 = document.getElementById("r_title")
    h1.innerText = "r/"+subreddit
    h1.parentElement.href = "/r/"+subreddit
}

function read_listing(obj){
    if(obj.kind == 'Listing'){
        return obj.data.children.map(read_child)
    }
    return []
}

function read_child(child){
    if(htmlgen.hasOwnProperty(child.kind))
        return htmlgen[child.kind](child)
    else
        return null
}


function get_vars(){
    // get hash
    let hash = window.location.hash.substr(1)

    // ojbect to return
    let obj = {}

    // check if hash is defined
    hash.split("&").forEach(x => {
        x = x.split("=")
        if(x[0] && x[0].length &&
           x[1] && x[1].length ){
            obj[x[0]] = x[1];
        }
    })

    return obj
}

function create_url(){
    let proxy  = "https://cors-anywhere.herokuapp.com/"
    let url    = "https://www.reddit.com"
    let path   = window.location.pathname
    let params = window.location.hash.substr(1)
    return proxy + url + path + ".json?raw_json=1&" + params
}

function encode_params(params){
    return Object.keys(params)
            .filter(key => params[key] && params[key].length)
            .map(key => key+"="+params[key])
            .join("&")
}


function json_fetch(url){
    return fetch(url)
                .then(res => res.json())
}

function html_make_post(child){
    // make post element
    let post = document.createElement("div");
    post.classList.add('post')

    // make thumbnail element
    if(child.data.thumbnail.startsWith("http") &&
      !(child.data.crosspost_parent_list && child.data.crosspost_parent_list.length)){
        let thumbnail = document.createElement("img");
        thumbnail.classList.add('thumbnail')
        thumbnail.setAttribute("src", child.data.thumbnail)

        if(child.data.post_hint == "image"){
            thumbnail.onclick = () => {
                if(thumbnail.classList.contains("thumbnail")){
                    thumbnail.src = child.data.url
                    thumbnail.classList.remove("thumbnail")
                    text_con.appendChild(thumbnail)
                }else{
                    thumbnail.src = child.data.thumbnail
                    thumbnail.classList.add("thumbnail")
                    post.appendChild(thumbnail)
                }
            }
            post.appendChild(thumbnail)
        }
        else if(child.data.post_hint == "hosted:video"){
            thumbnail.onclick = () => {
                post.removeChild(thumbnail)
                let vid = document.createElement('video')
                vid.src = child.data.media.reddit_video.scrubber_media_url
                text_con.appendChild(vid)
            }
        }
        else if(child.data.media_embed.content){
            thumbnail.onclick = () => {
                post.removeChild(thumbnail)
                let div = document.createElement('div')
                div.innerHTML = child.data.media_embed.content
                post.appendChild(div)
            }
            post.appendChild(thumbnail)
        }
        else if(child.data.preview && child.data.preview.reddit_video_preview){
            thumbnail.onclick = () => {
                post.removeChild(thumbnail)
                let vid = document.createElement('video')
                vid.src = child.data.preview.reddit_video_preview.fallback_url
                vid.autoplay = '1'
                vid.controls = true
                text_con.appendChild(vid)
            }
            post.appendChild(thumbnail)
        }
        else{
            let a = document.createElement('a')
            a.href = child.data.url
            a.target = "_BLANK"
            a.appendChild(thumbnail)
            post.appendChild(a)
        }
    }

    // make text continer
    let text_con = document.createElement("div");
    text_con.classList.add('text')
    post.appendChild(text_con);

    // make title element
    let title_con = document.createElement("div");
    title_con.classList.add('title')
    text_con.appendChild(title_con);

    // add title with link
    let title = title_con.appendChild(document.createElement('a'));
    title.href = child.data.permalink
    title.innerHTML = child.data.title

    let new_tab_link = title_con.appendChild(document.createElement('a'))
    new_tab_link.href = title.href;
    new_tab_link.target = "_BLANK";
    new_tab_link.classList.add('new_tab')
    new_tab_link.innerText = "↗";

    let json_btn = title_con.appendChild(document.createElement('a'))
    json_btn.href = "data:application/json,"+encodeURIComponent(JSON.stringify(child))
    json_btn.target = "_BLANK";
    json_btn.classList.add('new_tab')
    json_btn.innerText = ".json";

    // add link to subreddit in title
    let sub_r_link = title_con.appendChild(document.createElement('a'));
    sub_r_link.innerText = "r/"+child.data.subreddit
    sub_r_link.href = '/r/'+child.data.subreddit
    sub_r_link.classList.add('subreddit_link')

    // make desc element
    if(child.data.selftext){
        let desc = document.createElement("p");
        desc.classList.add('desc')
        desc.innerHTML = child.data.selftext_html
        text_con.appendChild(desc);
    }

    if(child.data.crosspost_parent_list && child.data.crosspost_parent_list.length){
        child.data.crosspost_parent_list.map(x => html_make_post({data: x})).map(e => post.appendChild(e))
    }

    return post
}

function html_make_comment(child){
    // make comment element
    let comment = document.createElement("div");
    comment.classList.add('comment')

    let content = document.createElement("div");
    content.classList.add('content')
    comment.appendChild(content)

    // make user element
    let user = document.createElement("div");
    user.classList.add('user')
    user.innerText = "u/"+child.data.author
    content.appendChild(user);

    // make text element
    let text = document.createElement("div");
    text.classList.add('text')
    text.innerHTML = child.data.body_html
    content.appendChild(text);

    // make text replies container
    if(child.data.replies){
        let rep_json =child.data.replies.data.children
        let rep_con = document.createElement("div");
        rep_con.classList.add('rep_con')

        if(false){
            rep_con.innerText = '[ more ]'
            rep_con.classList.add('collapsed')
        }
        else{
            rep_json.map(html_make_comment).forEach(e => rep_con.appendChild(e))
            comment.appendChild(rep_con);

            let line = document.createElement('div');
            line.classList.add('line')
            rep_con.appendChild(line)

            line.onclick = (e) => {
                comment.removeChild(rep_con);
                content.removeChild(text);

                //e.target.parentElement.classList.add('collapsed')
                //e.target.parentElement.innerText = '[ collapsed ]'
            }
        }
    }

    return comment
}

function html_add_elements(elements){
    let posts_con = document.getElementById("posts")
    elements.filter(e => e).map(e => posts_con.appendChild(e))
}

let htmlgen = {
    t1: html_make_comment,
    t3: html_make_post,
}
